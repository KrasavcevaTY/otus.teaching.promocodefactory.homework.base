﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> Add(T newData)
        {
            newData.Id = Guid.NewGuid();
            Data.Append(newData);
            return Task.FromResult(newData);
        }
        public void Delete(Guid id)
        {
            ((List<T>)Data).RemoveAll(x => x.Id == id);
        }

        public Task<T> Update(T newData)
        {
            if (newData == null)
                return null;
            T exist = Data.FirstOrDefault(x => x.Id == newData.Id);
            if (exist != null)
            {
                exist = newData;
            }
            return Task.FromResult(exist);
        }
    }
}